       //##     ////////////    ////////////     ////////////      ///////      ////      //    ##//
      //##     ////////////    ////       //    ////           ////     ///    ////      //    ##//
     //##     ////            ////       //    ////           ////            ////      //    ##//
    //##     ///////         /////////////    ////////          ///////      ////////////    ##//
   //##     ///////         ///////          ////////                ///    ////////////    ##//
  //##     ////            ////  ////       ////           ////     ///    ////      //    ##//
 //##     ////            ////     ////    ////////////      ///////      ////      //    ##//
//#########################################################################################//

#include "Arduino.h"
#include "Servo.h"
#include <Motor.h>
#include <Servos.h>
#include <Sensors.h>
#include <Speedometer.h>
#include <PID.h>
#include <Logic.h>

void setup() {
  Motor::init();
  Servos::init();
  Speedometer::init();
  Serial.begin(9600);
}

bool goBack = false;
int counter = 0;
int sp = 1548;

void loop() {
  double inter = Logic::addInter((int)Sensors::L, (int)Sensors::R);
  inter--;
  Sensors::recalculation();
  if (Speedometer::getSpeed() != (float)0 && counter == 0) {
    Sensors::recalculation();
    if (Logic::minValue(Sensors::L0, Sensors::R0) > 35) {
      Servos::setAngle(-2);
    } else {
      Servos::setAngle(-55 * Logic::sign(inter));
    }
  }
  else {
    if (counter < 80) {
      Motor::setSpeed(1460);
    } else if (counter < 110) {
      Motor::setSpeed(1500);
    } else if (Logic::minValue(Sensors::L0, Sensors::R0) < 35) {
      Motor::setSpeed(1420);
      Servos::setAngle(55 * Logic::sign(inter));
    } else {
      counter = 0;
    }
    counter++;
  }  
  delay(20);
}
